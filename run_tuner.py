from pathlib import Path
import approxtuner as at
from approxtuner.model_zoo import CIFAR, VGG16Cifar10
import torch
from torch.utils.data import DataLoader

output_dir = Path("approxtuner_logs/vgg16_acc=3")
msg_logger = at.config_pylogger(output_dir=output_dir, verbose=True)

prefix = Path("model_params/cifar10")
tune_set = CIFAR.from_file(prefix / "tune_input.bin", prefix / "tune_labels.bin")
tune_loader = DataLoader(tune_set, batch_size=500)
test_set = CIFAR.from_file(prefix / "test_input.bin", prefix / "test_labels.bin")
test_loader = DataLoader(test_set, batch_size=500)
module = VGG16Cifar10()
module.load_state_dict(torch.load("model_params/vgg16_cifar10.pth.tar"))

app = at.TorchApp(
    "TestTorchApp",  # Application name -- can be anything
    module,
    tune_loader,
    test_loader,
    knobs=at.get_knobs_from_file(),
    tensor_to_qos=at.accuracy,
    model_storage_folder="approxtuner_logs/",
)
tuner = app.get_tuner()
# TODO: 1. Add argument below to use a QoS model to predict the accuracy of model
tuner.tune(
    max_iter=5000,
    # TODO: 3. Change QoS threshold here.
    #       Check out documentation to see why qos_tuner_threshold is lower than qos_keep_threshold.
    #       When you change the threshold, you should always keep qos_tuner_threshold slightly
    #       lower than qos_keep_threshold.
    qos_tuner_threshold=2.5,  # QoS threshold to guide tuner into
    qos_keep_threshold=3.0,  # QoS threshold for which we actually keep the configurations
    is_threshold_relative=True,  # Thresholds are relative to baseline -- baseline_acc - 2.0
    take_best_n=20,
    cost_model="cost_linear",  # Use linear cost predictor
)
tuner.dump_configs(output_dir / "configs.json", tuner.all_configs)
fig = tuner.plot_configs(show_qos_loss=True)
fig.savefig(output_dir / "configs.png")
