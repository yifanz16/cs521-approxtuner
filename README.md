# Autotuning for Adaptive Approximations

[ApproxTuner](https://dl.acm.org/doi/10.1145/3437801.3446108) is an automatic framework for
approximations of tensor-based applications (such as DNNs).
ApproxTuner finds approximation choices automatically by autotuning,
while significantly speeds up autotuning by analytically predicting the accuracy impacts of approximations.

We'll now get started with ApproxTuner by tuning a DNN as example.

## Environment Setup

1. Install Conda, a python virtual environment manager from [here](https://docs.conda.io/en/latest/miniconda.html).
1. Using the environment description `./env.yaml` provided, create a new python environment:
   ```bash
   conda env create -n approxtuner -f ./env.yaml
   ```
   This creates the conda environment `approxtuner`.
1. Activate the python environment:
   ```bash
   conda activate approxtuner
   ```
   - Make sure to do so for every `bash` shell you want to run ApproxTuner in.
1. Download pretrained DNN weights from [here](https://drive.google.com/file/d/1KoUkj6AZAPEBNEy1VEVFYcB8NQ1uGSAG/view)
   and extract into the root of repository:
   ```
   tar xzf vgg16_small.tar.gz
   ```
   This should create a directory `model_params/`.

## Tuning VGG16 on CIFAR-10

1. Run `./run_tuner.py` directly.
   This will start a tuning process which also produces a progress bar:
   ```
   0%|▏                              | 40/5000 [00:18<39:05,  2.44it/s]
   ```

   This script runs autotuning _empirically_ for 5000 iterations:
   every iteration the DNN is approximated and run over the validation set to get the accuracy.
   Empirical autotuning can be very time-consuming.

   - You don't need to finish running this script;
     if everything runs and you see a progress bar progressing,
     your environment setup is intact.
   - This will take 30~40 minutes on an RTX 2080.

1. Instead, let's now use _predictive_ autotuning.
   Checkout the guide in the documentation [here](https://predtuner.readthedocs.io/en/latest/getting_started.html)
   and find how to use an accuracy predictor for tuning (called a "QoS model").

   - Also see `# TODO: 1` in `./run_tuner.py`. Same for the following tasks.
   
   Run the script after modifying it.
   This should produce a directory `approxtuner_logs/` with a figure
   and a JSON file `configs.json` with all the configurations.
   Refer to the [documentation](https://predtuner.readthedocs.io/en/latest/getting_started.html)
   for how to read the figure.

1. Now, `configs.json` contains a list of [`ValConfig`](https://predtuner.readthedocs.io/en/latest/reference/modeled-app.html#predtuner.modeledapp.ValConfig)
   that you can read back and analyse.

   Complete the code in `plot_tuning_progress.py` (see `TODO: 2` in that file) to
   draw a figure of iteration number $n$ (x-axis) v.s.
   the best speedup obtained in the first $n$ iterations (y-axis)
   using the information in `configs.json`.

   - The file should contain exactly as many configurations as iterations,
     so a configuration at index $i$ is the configuration of iteration $i$.

   - Some configurations in the list have low accuracy (QoS),
     so they should not count as valid speedup.
     Remember to filter them out when plotting.

   - You can find what the QoS threshold should be in the output of tuner script:
     ```
     Tuner QoS threshold: 86.720000; keeping configurations with QoS >= 86.220000 (tune dataset)
     ```
     Then you can filter out all configurations with QoS lower than `86.220000`.
     (Use `config.validated_qos` as the QoS of the config.)

1. Modify `./run_tuner.py` to repeat this once more for a different QoS threshold:
   `qos_tuner_threshold=4.5, qos_keep_threshold=5`, (see `TODO: 3` in that file)
   and use your `plot_tuning_progress.py` to plot the tuning progress similarly.

   - You may also want to change the output directory for this new run.
     Change `output_dir` in `run_tuner.py` to something else, such as `output_dir = Path("approxtuner_logs/vgg16_acc=5")`.

## Deliverable

Please include the following files you modify:  run_tuner.py, plot_tuning_progress.py,
plus the 2 figures for 2 different accuracy thresholds that your `plot_tuning_progress.py` generates.
Please name the figures descriptively so these figures correspond to their accuracy threshold.
